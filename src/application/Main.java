package application;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
  
  @Override
  public void start(Stage primaryStage) throws IOException {
    ResourceBundle bundle = ResourceBundle.getBundle("bundles.labels", new Locale("fr", "FR"));
    
    Parent root = FXMLLoader.load(Main.class.getResource("/application/views/MainView.fxml"), bundle);
  
    Scene scene = new Scene(root);
    scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
    
    primaryStage.setTitle("MyClassroom");
    primaryStage.setScene(scene);
    primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/myclassroom.png")));
    primaryStage.show();
  }
  
  public static void main(String[] args) {
      launch(args);
  }

}