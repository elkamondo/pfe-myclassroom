package application.controllers;

import static application.helpers.FormUtils.extractFieldList;
import static application.helpers.FormUtils.extractFieldValue;
import static application.helpers.FormUtils.hasValidFieldValue;
import static application.helpers.FormUtils.hasValidFieldList;
import static application.helpers.FormUtils.hasValidSpreadsheetId;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import application.helpers.DisplayNotification;
import application.models.GSheet;
import application.validation.GException;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class AddFieldsFormController implements Initializable {
  
  private ResourceBundle bundle;

  private Stage loadingStage;
  
  @FXML
  private JFXTextField sourceKeyField;

  @FXML
  private JFXTextField destinationKeyField;
  
  @FXML
  private JFXTextField valuesField;
  
  @FXML
  private JFXTextField destinationColumnField;
  
  @FXML
  private JFXTextField spreadsheetIdSourceField;
  
  @FXML
  private JFXTextField spreadsheetIdDestinationField;
  
  @FXML
  private Label sourceKeyError;

  @FXML
  private Label destinationKeyError;
  
  @FXML
  private Label valuesError;

  @FXML
  private Label destinationColumnError;

  @FXML
  private Label spreadsheetIdSourceError;

  @FXML
  private Label spreadsheetIdDestinationError;

  @FXML
  private JFXButton resetButton;

  @FXML
  private JFXButton addFieldsButton;

  @Override
  public void initialize(URL url, ResourceBundle bundle) {
    this.bundle = bundle;
    loadingStage = DisplayNotification.loadingStage(bundle.getString("addFieldsLoadingMessage"));
  }

  @FXML
  private void addFieldsToSpreadsheet(ActionEvent event) {
    if (validateForm()) {
      hideAllErrors();

      String sourceId = spreadsheetIdSourceField.getText().trim();
      String destinationId = spreadsheetIdDestinationField.getText().trim();
      List<Character> sourceKeyList = extractFieldList(sourceKeyField);
      List<Character> destinationKeyList = extractFieldList(destinationKeyField);
      List<Character> valuesFieldList = extractFieldList(valuesField);
      char columnWhereToAdd = extractFieldValue(destinationColumnField);

      GSheet spreadsheetSource = new GSheet(sourceId);
      GSheet spreadsheetDestination = new GSheet(destinationId);
      
      Task<Void> task1 = new Task<Void>() {
        @Override
        protected Void call() throws GException {
          spreadsheetSource.collectInfo(sourceKeyList, valuesFieldList, true);
          return null;
        }
      };
      task1.setOnRunning(e -> loadingStage.show());
      task1.setOnFailed(e -> {
        loadingStage.close();
        DisplayNotification.error(task1);
      });
      
      Task<Void> task2 = new Task<Void>() {
        @Override
        protected Void call() throws GException {
          spreadsheetDestination.collectInfo(destinationKeyList, Collections.emptyList(), true);
          return null;
        }
      };
      task2.setOnFailed(e -> {
        loadingStage.close();
        DisplayNotification.error(task2);
      });
      
      Task<Void> task3 = new Task<Void>() {
        @Override
        protected Void call() {
          spreadsheetDestination.updateValues(spreadsheetSource.getValues());
          return null;
        }
      };
      
      Task<Void> task4 = new Task<Void>() {
        @Override
        protected Void call() throws GException {
          spreadsheetDestination.saveValues(columnWhereToAdd, spreadsheetSource.getTitleBar(), false);
          return null;
        }
      };
      task4.setOnFailed(e -> {
        loadingStage.close();
        DisplayNotification.error(task4);
      });

      new Thread(task1).start();
      
      task1.setOnSucceeded(e -> new Thread(task2).start());
      task2.setOnSucceeded(e -> new Thread(task3).start());
      task3.setOnSucceeded(e -> new Thread(task4).start());
      task4.setOnSucceeded(e -> {
        loadingStage.close();
        DisplayNotification.success(bundle.getString("addFieldsSuccessMessage"));
      });
    } else {
      spreadsheetIdSourceError.setVisible(!hasValidSpreadsheetId(spreadsheetIdSourceField));
      spreadsheetIdDestinationError.setVisible(!hasValidSpreadsheetId(spreadsheetIdDestinationField));
      sourceKeyError.setVisible(!hasValidFieldList(sourceKeyField));
      destinationKeyError.setVisible(!hasValidFieldList(destinationKeyField));
      valuesError.setVisible(!hasValidFieldList(valuesField));
      destinationColumnError.setVisible(!hasValidFieldValue(destinationColumnField));
    }
  }

  @FXML
  private void resetForm(ActionEvent event) {
    hideAllErrors();

    sourceKeyField.clear();
    destinationKeyField.clear();
    valuesField.clear();
    destinationColumnField.clear();
    spreadsheetIdSourceField.clear();
    spreadsheetIdDestinationField.clear();
  }

  private boolean validateForm() {
    return hasValidSpreadsheetId(spreadsheetIdSourceField)
        && hasValidSpreadsheetId(spreadsheetIdDestinationField)
        && hasValidFieldList(sourceKeyField)
        && hasValidFieldList(destinationKeyField) 
        && hasValidFieldList(valuesField)
        && hasValidFieldValue(destinationColumnField);
  }
  
  private void hideAllErrors() {
    spreadsheetIdSourceError.setVisible(false);
    spreadsheetIdDestinationError.setVisible(false);
    sourceKeyError.setVisible(false);
    destinationKeyError.setVisible(false);
    valuesError.setVisible(false);
    destinationColumnError.setVisible(false);
  }

}
