package application.controllers;

import static application.helpers.FormUtils.hasValidSpreadsheetId;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.services.drive.Drive;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;

import application.helpers.DisplayNotification;
import application.models.Authentication;
import application.models.GDrive;
import application.validation.GException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class ImportFilesFormController implements Initializable {
  
  private static final Logger LOGGER = Logger.getLogger(ImportFilesFormController.class.getName());

  private List<java.io.File> selectedFiles = new ArrayList<>();
  
  private ObservableList<String> fileNamesList = FXCollections.observableArrayList();
  
  private ResourceBundle bundle;
  
  private Stage loadingStage;
  
  @FXML
  private JFXTextField destinationFolderId;

  @FXML
  private Text noFilesLabel;
  
  @FXML
  private Label destinationFolderIdError;

  @FXML
  private Label selectFilesError;
  
  @FXML
  private JFXListView<String> selectedFilesListView;
  
  @FXML
  private JFXButton resetButton;

  @FXML
  private JFXButton selectFilesBtn;
  
  @Override
  public void initialize(URL url, ResourceBundle bundle) {
    this.bundle = bundle;
    
    try {
      Drive driveService = Authentication.getDriveService();
      GDrive.setService(driveService);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    loadingStage = DisplayNotification.loadingStage(bundle.getString("importFilesLoadingMessage"));
    
    selectedFilesListView.setItems(fileNamesList);
  }
  
  @FXML
  private void selectFiles(ActionEvent event) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle(bundle.getString("importYourFiles"));
    fileChooser.setInitialDirectory(new java.io.File(System.getProperty("user.home")));
    fileChooser.getExtensionFilters().add(new ExtensionFilter("Tous les fichiers", "*.*"));
    
    List<java.io.File> choosedFiles = fileChooser.showOpenMultipleDialog(null);
    if (choosedFiles != null) {
      selectedFiles.addAll(choosedFiles);
      
      int numberOfSelectedFiles = selectedFiles.size();
      noFilesLabel.setText(
          String.format("(%d) fichier%s séléctionné%s.",
             numberOfSelectedFiles,
             numberOfSelectedFiles > 1 ? "s sont" : " est",
             numberOfSelectedFiles > 1 ? "s" : ""
          )
      );
      
      List<String> fileNames = selectedFiles.stream().map(java.io.File::getName).collect(toList());
      
      fileNamesList.setAll(fileNames);
      selectedFilesListView.setVisible(true);
    }
  }
  
  @FXML
  private void uploadToDrive(ActionEvent event) {
    if (validateForm()) {
      hideAllErrors();
      
      String destinationId = destinationFolderId.getText().trim();
      GDrive folderDrive = new GDrive(destinationId);
      
      Task<Void> task = new Task<Void>() {
        @Override
        protected Void call() throws GException {
          folderDrive.uploadFiles(selectedFiles);
          return null;
        }
      };
      task.setOnRunning(e -> loadingStage.show());
  
      task.setOnSucceeded(e -> {
        loadingStage.close();
        DisplayNotification.success(bundle.getString("importFilesSuccessMessage"));
      });
      
      task.setOnFailed(e -> {
        loadingStage.close();
        DisplayNotification.error(task);
      });
      
      new Thread(task).start();
    } else {      
      destinationFolderIdError.setVisible(!hasValidSpreadsheetId(destinationFolderId));
      selectFilesError.setVisible(selectedFiles.isEmpty());
    }
  }

  @FXML
  private void resetForm(ActionEvent event) {
    hideAllErrors();
    
    destinationFolderId.clear();
    noFilesLabel.setText(bundle.getString("noSelectedFiles"));
    selectedFiles.clear();
    fileNamesList.clear();
    selectedFilesListView.setVisible(false);
  }

  private boolean validateForm() {
    return hasValidSpreadsheetId(destinationFolderId) && !selectedFiles.isEmpty();
  }
  
  private void hideAllErrors() {
    destinationFolderIdError.setVisible(false);
    selectFilesError.setVisible(false);
  }

}
