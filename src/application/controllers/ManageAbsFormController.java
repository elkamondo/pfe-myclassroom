package application.controllers;

import static application.helpers.FormUtils.extractFieldValue;
import static application.helpers.FormUtils.hasValidFieldValue;
import static application.helpers.FormUtils.hasValidSpreadsheetId;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.sheets.v4.Sheets;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import application.helpers.DisplayNotification;
import application.models.Authentication;
import application.models.GDrive;
import application.models.GSheet;
import application.validation.GException;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ManageAbsFormController implements Initializable {
  
  private static final Logger LOGGER = Logger.getLogger(ManageAbsFormController.class.getName());

  private ResourceBundle bundle;
  
  private Stage loadingStage;
  
  @FXML
  private JFXTextField partialSourceKey;

  @FXML
  private JFXTextField partialSourceFolderIdField;
  
  @FXML
  private JFXTextField partialDestinationIdField;
  
  @FXML
  private Label partialSourceKeyError;

  @FXML
  private Label partialSourceFolderIdError;

  @FXML
  private Label partialDestinationIdError;

  @FXML
  private JFXButton partialResetButton;

  @FXML
  private JFXButton partialCalculateButton;

  @FXML
  private JFXTextField globalSpreadsheetTP;

  @FXML
  private JFXTextField globalSpreadsheetTD;
  
  @FXML
  private JFXTextField globalSpreadsheetCourse;
  
  @FXML
  private JFXTextField globalSpreadsheetDestinationId;
  
  @FXML
  private Label globalSpreadsheetTPError;

  @FXML
  private Label globalSpreadsheetTDError;

  @FXML
  private Label globalSpreadsheetCourseError;

  @FXML
  private Label globalSpreadsheetDestinationIdError;

  @FXML
  private JFXButton globalResetButton;

  @FXML
  private JFXButton globalCalculateButton;
  
  @Override
  public void initialize(URL url, ResourceBundle bundle) {
    this.bundle = bundle;
    
    try {
      Sheets sheetsService = Authentication.getSheetsService();
      Drive driveService = Authentication.getDriveService();

      GSheet.setService(sheetsService);
      GDrive.setService(driveService);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
    
    loadingStage = DisplayNotification.loadingStage(bundle.getString("manageClassLoadingMessage"));
  }

  @FXML
  private void calculatePartialAttendances(ActionEvent event) {
    if (validateFirstTabForm()) {
      hideAllFirstTabFormErrors();

      String folderId = partialSourceFolderIdField.getText().trim();
      String destinationId = partialDestinationIdField.getText().trim();
      char sourceKey = extractFieldValue(partialSourceKey);

      GDrive attendanceSheetsFolder = new GDrive(folderId);

      List<File> spreadsheets = new ArrayList<>();
      
      Task<Void> task1 = new Task<Void>() {
        @Override
        protected Void call() throws GException {
          spreadsheets.addAll(attendanceSheetsFolder.getSpreadsheetsOnly());
          return null;
        }
      };
      task1.setOnRunning(e -> loadingStage.show());
      task1.setOnFailed(e -> {
        loadingStage.close();
        DisplayNotification.error(task1);
      });
      
      Task<Void> task2 = new Task<Void>() {
        @Override
        protected Void call() throws GException {
          List<String> spreadsheetsIds = spreadsheets.stream().map(File::getId).collect(toList());
          GSheet.saveAttendees(spreadsheetsIds, sourceKey, null, destinationId);
          return null;
        }
      };
      task2.setOnFailed(evt -> {
        loadingStage.close();
        DisplayNotification.error(task2);
      });
      
      new Thread(task1).start();
      
      task1.setOnSucceeded(e -> new Thread(task2).start());
      task2.setOnSucceeded(e -> {
        loadingStage.close();
        DisplayNotification.success(bundle.getString("manageClassSuccessMessage"));
      });
    } else {
      partialSourceKeyError.setVisible(!hasValidFieldValue(partialSourceKey));
      partialSourceFolderIdError.setVisible(!hasValidSpreadsheetId(partialSourceFolderIdField));
      partialDestinationIdError.setVisible(!hasValidSpreadsheetId(partialDestinationIdField));
    }
  }

  @FXML
  private void calculateGlobalAttendances(ActionEvent event) {
    if (validateSecondTabForm()) {
      hideAllSecondTabFormErrors();

      String srcIdTP = globalSpreadsheetTP.getText().trim();
      String srcIdTD = globalSpreadsheetTD.getText().trim();
      String srcIdCourse = globalSpreadsheetCourse.getText().trim();
      String destinationId = globalSpreadsheetDestinationId.getText().trim();

      List<String> attendanceSheets = Arrays.asList(srcIdTP, srcIdTD, srcIdCourse);

      Task<Void> task = new Task<Void>() {
        @Override
        protected Void call() throws GException {
          GSheet.saveAttendees(attendanceSheets, 'A', 'B', destinationId);
          return null;
        }
      };
      task.setOnRunning(e -> loadingStage.show());
      
      task.setOnFailed(e -> {
        loadingStage.close();
        DisplayNotification.error(task);
      });
      
      task.setOnSucceeded(e -> {
        loadingStage.close();
        DisplayNotification.success(bundle.getString("manageClassSuccessMessage"));
      });
      
      new Thread(task).start();
    } else {
      globalSpreadsheetTPError.setVisible(!hasValidSpreadsheetId(globalSpreadsheetTP));
      globalSpreadsheetTDError.setVisible(!hasValidSpreadsheetId(globalSpreadsheetTD));
      globalSpreadsheetCourseError.setVisible(!hasValidSpreadsheetId(globalSpreadsheetCourse));
      globalSpreadsheetDestinationIdError.setVisible(!hasValidSpreadsheetId(globalSpreadsheetDestinationId));
    }
  }

  @FXML
  private void resetFirstTabForm(ActionEvent event) {
    hideAllFirstTabFormErrors();

    partialSourceKey.clear();
    partialSourceFolderIdField.clear();
    partialDestinationIdField.clear();
  }

  @FXML
  private void resetSecondTabForm(ActionEvent event) {
    hideAllSecondTabFormErrors();

    globalSpreadsheetTP.clear();
    globalSpreadsheetTD.clear();
    globalSpreadsheetCourse.clear();
    globalSpreadsheetDestinationId.clear();
  }

  private boolean validateFirstTabForm() {
    return hasValidFieldValue(partialSourceKey)
        && hasValidSpreadsheetId(partialSourceFolderIdField)
        && hasValidSpreadsheetId(partialDestinationIdField);
  }

  private boolean validateSecondTabForm() {
    return hasValidSpreadsheetId(globalSpreadsheetTP) 
        && hasValidSpreadsheetId(globalSpreadsheetTD)
        && hasValidSpreadsheetId(globalSpreadsheetCourse)
        && hasValidSpreadsheetId(globalSpreadsheetDestinationId);
  }

  private void hideAllFirstTabFormErrors() {
    partialSourceKeyError.setVisible(false);
    partialSourceFolderIdError.setVisible(false);
    partialDestinationIdError.setVisible(false);
  }

  private void hideAllSecondTabFormErrors() {
    globalSpreadsheetTPError.setVisible(false);
    globalSpreadsheetTDError.setVisible(false);
    globalSpreadsheetCourseError.setVisible(false);
    globalSpreadsheetDestinationIdError.setVisible(false);
  }

}
