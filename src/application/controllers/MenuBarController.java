package application.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;

public class MenuBarController implements Initializable {
  
  private static final Logger LOGGER = Logger.getLogger(MenuBarController.class.getName());

  private Parent homePage;
  
  private Parent addFieldsPage;
  
  private Parent manageAbsPage;
  
  private Parent importPage;
  
  private Parent helpPage;
  
  @FXML
  private StackPane stackPane;

  @FXML
  private JFXButton homeAction;

  @FXML
  private JFXButton addFieldsAction;

  @FXML
  private JFXButton manageAbsenceAction;

  @FXML
  private JFXButton importFilesAction;

  @FXML
  private JFXButton helpAction;
  
  @Override
  public void initialize(URL arg0, ResourceBundle arg1) {
    ResourceBundle bundle = ResourceBundle.getBundle("bundles.labels", new Locale("fr", "FR"));
    
    try {
      homePage = FXMLLoader.load(getClass().getResource("/application/views/HomePage.fxml"), bundle);
      addFieldsPage = FXMLLoader.load(getClass().getResource("/application/views/AddFieldsPage.fxml"), bundle);
      manageAbsPage = FXMLLoader.load(getClass().getResource("/application/views/ManageAbsPage.fxml"), bundle);
      importPage = FXMLLoader.load(getClass().getResource("/application/views/ImportFilesPage.fxml"), bundle);
      helpPage = FXMLLoader.load(getClass().getResource("/application/views/HelpPage.fxml"), bundle);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }
  
  @FXML
  private void renderHomePage(ActionEvent event) {
    stackPane.getChildren().clear();
    stackPane.getChildren().add(homePage);
  }
  
  @FXML
  private void renderAddFieldsPage(ActionEvent event) {
    stackPane.getChildren().clear();
    stackPane.getChildren().add(addFieldsPage);
  }
  
  @FXML
  private void renderManageAbsPage(ActionEvent event) {
    stackPane.getChildren().clear();
    stackPane.getChildren().add(manageAbsPage);
  }
  
  @FXML
  private void renderImportFilesPage(ActionEvent event) {
    stackPane.getChildren().clear();
    stackPane.getChildren().add(importPage);
  }
  
  @FXML
  private void renderHelpPage(ActionEvent event) {
    stackPane.getChildren().clear();
    stackPane.getChildren().add(helpPage);
  }
  
}
