package application.helpers;

import javafx.scene.control.Alert;

public class AlertNotification extends Alert {

  public AlertNotification(AlertType alertType) {
    super(alertType);
  }
  
  public AlertNotification(AlertType alertType, String title, String content) {
    super(alertType);
    this.setTitle(title);
    this.setHeaderText(null);
    this.setContentText(content);
  }

}
