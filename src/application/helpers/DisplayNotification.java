package application.helpers;

import application.validation.GException;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class DisplayNotification {

  private DisplayNotification() {}
  
  public static <T> void error(Task<T> task) {
    Throwable throwedException = task.getException();
    
    if (throwedException instanceof GException) {
      GException e = (GException)throwedException;
      new AlertNotification(AlertType.ERROR, "Erreur", e.getErrorMessage()).show();
    } else {
      new AlertNotification(AlertType.ERROR, "Erreur", throwedException.toString()).show();
    }
  }
  
  public static <T> void warning(Task<T> task) {
    Throwable throwedException = task.getException();
    
    if (throwedException instanceof GException) {
      GException e = (GException)throwedException;
      new AlertNotification(AlertType.WARNING, "Avertissement", e.getErrorMessage()).show();
    } else {
      new AlertNotification(AlertType.WARNING, "Avertissement", throwedException.toString()).show();
    }
  }
  
  public static void success(String message) {
    new AlertNotification(AlertType.INFORMATION, "Succès", message).show();
  }
  
  public static Stage loadingStage(String message) {
    Label label = new Label(message);
    label.setFont(Font.font("Lato Medium", 18));
    label.setTextAlignment(TextAlignment.CENTER);
    
    ProgressIndicator progressIndicator = new ProgressIndicator();
    progressIndicator.setPrefHeight(30);
    progressIndicator.setPrefWidth(30);
    
    HBox root = new HBox(10);
    root.setAlignment(Pos.CENTER);
    root.getChildren().addAll(label, progressIndicator);
    
    Stage loadingStage = new Stage();
    loadingStage.setScene(new Scene(root, 400, 100));
    loadingStage.setResizable(false);
    loadingStage.setTitle(message);
    
    return loadingStage;
  }
  
}
