package application.helpers;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.jfoenix.controls.JFXTextField;

public class FormUtils {
  
  private FormUtils() {}

  public static boolean hasValidSpreadsheetId(JFXTextField field) {
    String fieldValue = field.getText().trim();
    if (fieldValue.isEmpty()) {
      return false;
    }
    
    return fieldValue.matches("([a-zA-Z0-9-_]+)");
  }
  
  public static boolean hasValidFieldList(JFXTextField field) {
    String fieldValue = field.getText().trim().toUpperCase();
    if (fieldValue.isEmpty()) {
      return false;
    }
    
    Predicate<String> predicate = value -> value.length() == 1 && Character.isLetter(value.charAt(0));
    return Stream.of(fieldValue.split(",")).allMatch(predicate);
  }
  
  public static boolean hasValidFieldValue(JFXTextField field) {
    String fieldValue = field.getText().trim().toUpperCase();
    if (fieldValue.isEmpty()) {
      return false;
    }

    return fieldValue.length() == 1 && Character.isLetter(fieldValue.charAt(0));
  }
  
  public static List<Character> extractFieldList(JFXTextField field) {
    String[] values = field.getText().trim().toUpperCase().split(",");
    if (values.length == 0) {
      return Collections.emptyList();
    }
    
    return Stream.of(values).map(value -> value.charAt(0)).distinct().collect(Collectors.toList());
  }
  
  public static char extractFieldValue(JFXTextField field) {
    String value = field.getText().trim().toUpperCase();
    if (value.isEmpty()) {
      return Character.MIN_VALUE;
    }
    
    return value.charAt(0);
  }
}
