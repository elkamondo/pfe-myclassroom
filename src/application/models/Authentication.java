package application.models;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.sheets.v4.Sheets;

/**
 * <b>
 *  La classe Authentication est la classe responsable de l’autorisation aux services 
 *  Google Sheets API et Google Drive API.
 * </b>
 * 
 * @author Mohcine EL KASSIB, Abdrrezzak ATIFI
 * @since v1.0
 */
public class Authentication {

  /**
   * Un logger utilisé pour afficher le journal d'événements.
   */
  private static final Logger LOGGER = Logger.getLogger(Authentication.class.getName());
  
  /**
   * Le nom de l'application.
   * 
   * @see #getSheetsService()
   * @see #getDriveService()
   */
  private static final String APPLICATION_NAME = "MyClassroom";
  
  /**
   * Le répértoire pour stocker les crédentiels d'utilisateur.
   */
  private static final File DATA_STORE_DIR = new File(System.getProperty("user.home"), ".credentials/app.myclassroom");

  /**
   * L'extracteur des crédentiels stockés.
   * 
   * @see Authentication#authorize()
   */
  private static FileDataStoreFactory dataStoreFactory;

  /**
   * L'extracteur des objets JSON.
   * 
   * @see #authorize()
   * @see #getSheetsService()
   * @see #getDriveService()
   */
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();


  /**
   * Le transport du client HTTP.
   * 
   * @see #authorize()
   * @see #getSheetsService()
   * @see #getDriveService()
   */
  private static HttpTransport httpTransport;

  /**
   * Les privilèges d'accès requis pour cette application.
   * 
   * <p>
   *  Si vous avez changé les privilèges d'accès, supprimez les anciens stockés
   *  dans le répértoire ~/.credentials/app.myclassroom.
   * </p>
   * <p>
   *  Pour plus d'informations: <a href="https://developers.google.com/sheets/api/guides/authorizing" target="_blank">
   *  Les privilèges d'accès
   *  </a>
   * </p>
   * 
   * @see #authorize()
   */
  private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
  
  static {
    try {
      httpTransport = GoogleNetHttpTransport.newTrustedTransport();
      dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
    } catch (GeneralSecurityException e) {
      LOGGER.log(Level.SEVERE, "Error while setting up the security");
      System.exit(1);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Error handling resources files");
      System.exit(1);
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, "Error while getting the AccesToken");
      System.exit(1);
    }
  }
  
  private Authentication() {}

  /**
   * Obtenir le service d'autorization d'accès à Google Sheets API.
   * 
   * @return Le service d'autorization d'accès à Google Sheets API.
   * 
   * @throws IOException
   * 
   * @see GSheet#setService(Sheets)
   */
  public static Sheets getSheetsService() throws IOException {
    Credential credential = authorize();
    return new Sheets.Builder(httpTransport, JSON_FACTORY, credential)
      .setApplicationName(APPLICATION_NAME)
      .build();
  }
  
  /**
   * Obtenir le service d'autorization d'accès à Google Drive API.
   * 
   * @return Le service d'autorization d'accès à Google Drive API.
   * 
   * @throws IOException
   * 
   * @see GDrive#setService(Drive)
   */
  public static Drive getDriveService() throws IOException {
    Credential credential = authorize();
    return new Drive.Builder(httpTransport, JSON_FACTORY, credential)
      .setApplicationName(APPLICATION_NAME)
      .build();
  }

  /**
   * Construire l'objet des crédentiels.
   * 
   * @return L'objet des crédentiels
   * 
   * @throws IOException
   * 
   * @see #getSheetsService()
   * @see #getDriveService()
   */
  private static Credential authorize() throws IOException {
    InputStream in = Authentication.class.getResourceAsStream("/client_secrets.json");
    GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

    GoogleAuthorizationCodeFlow flow =
      new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets, SCOPES)
      .setDataStoreFactory(dataStoreFactory)
      .setAccessType("offline")
      .build();

    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
  }
}