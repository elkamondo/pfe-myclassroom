package application.models;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import application.validation.GException;

/**
 * <b>GDrive est la classe représentant d'un dossier dans Google Drive.</b>
 * <p>Un GDrive est caractérisé par un identifiant unique.</p>
 * 
 * <p>
 *  Pour avoir l'accès au dossier Drive, il faudra que l'utilisateur s'authentifie
 *  à travers le service d'authentification qui permet l'accès au compte Google Drive.
 * </p>
 * 
 * @author Mohcine EL KASSIB
 * @since v1.0
 * 
 * @see Authentication
 */
public class GDrive {
  
  /**
   * Un logger utilisé pour afficher le journal d'événements.
   */
  private static final Logger LOGGER = Logger.getLogger(GDrive.class.getName());
  
  /**
   * Le service d'authentification qui permet l'accès au compte Google Drive.
   * 
   * <p>
   *  Il est définit comme un attribut {@code static} parce que toutes les instances
   *  de GDrive partage le même service d'authentification au compte Google Drive.
   * <p>
   * 
   * @see #setService(Drive)
   * @see #getFiles()
   * @see #uploadSingleFile(java.io.File)
   */
  private static Drive service;

  /**
   * L'identifiant unique du dossier Drive.
   * 
   * @see GDrive#GDrive(String)
   */
  private String folderId;
  
  /**
   * Constructeur GDrive.
   * 
   * @param folderId L'identifiant unique du dossier Drive.
   * 
   * @see #folderId
   */
  public GDrive(String folderId) {
    this.folderId = folderId;
  }
  
  /**
   * Obtenir l'identifiant du dossier Drive.
   * 
   * @return L'identifiant du dossier Drive
   * 
   * @see #folderId
   */
  public String getFolderId() {
    return folderId;
  }
  
  /**
   * Mettre à jour l'identifiant de la feuille de calcul.
   * 
   * @param folderId Le nouveau identifiant
   * 
   * @see #folderId
   */
  public void setFolderId(String folderId) {
    this.folderId = folderId;
  }
  
  /**
   * Obtenir le service d'authentification qui permet l'accès au compte Google Drive.
   * 
   * @return {@link #service}
   * 
   * @see #service
   */
  public static Drive getService() {
    return service;
  }
  
  /**
   * Mettre à jour le service d'accès au compte Google Drive.
   * 
   * @param service Le nouveau service d'accès au compte Google Drive
   * 
   * @see #service
   */
  public static void setService(Drive service) {
    GDrive.service = service;
  }
  
  /**
   * Retourner la liste de tous les fichiers de ce dossier Drive.
   * 
   * @return La liste des fichiers
   * 
   * @throws GException Si la requête est mal formée.
   * 
   * @see #folderId
   * @see #service
   * @see #createExceptionForGDrive(GoogleJsonResponseException)
   */
  public List<File> getFiles() throws GException {
    List<File> listing = new ArrayList<>();
    
    String pageToken = null;
    do {
      // Construire la requête de la recherche des fichiers
      String query = 
          "'" + folderId + "' in parents and mimeType != 'application/vnd.google-apps.folder' and trashed = false";
      
      // Exécution de la requête
      FileList result = null;
      try {
        result =
          service.files().list()
            .setQ(query)
            .setSpaces("drive")
            .setFields("nextPageToken, files(id, name, mimeType)")
            .setPageToken(pageToken)
            .execute();
      } catch (GoogleJsonResponseException e) {
        throw createExceptionForGDrive(e);
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, e.getMessage(), e);
      }
      
      if (result != null) {
        listing.addAll(result.getFiles());
        
        pageToken = result.getNextPageToken();
      }
    } while (pageToken != null);
    
    return listing;
  }
  
  /**
   * Retourne seulement la liste des spreadsheets (Google Sheets) de ce dossier Drive.
   * 
   * @return La liste des spreadsheets.
   * 
   * @throws GException Si la requête est mal formée.
   * 
   * @see #getFiles()
   */
  public List<File> getSpreadsheetsOnly() throws GException {
    return getFiles().stream()
        .filter(file -> file.getMimeType().equals("application/vnd.google-apps.spreadsheet"))
        .collect(toList());
  }
  
  /**
   * Transférer des fichiers vers ce dossier Drive.
   * 
   * @param filesToUpload Les fichiers à transférer
   *                      
   * @throws GException Si les fichiers ont déjà dans ce dossier Drive.
   * 
   * @see #getFiles()
   * @see #uploadSingleFile(java.io.File)
   */
  public void uploadFiles(List<java.io.File> filesToUpload) throws GException {
    List<String> fileNamesInDriveFolder = getFiles().stream().map(File::getName).collect(toList());

    // Récupérer la liste des fichiers qui ne sont pas dans le dossier Drive
    List<java.io.File> filteredFiles = filesToUpload.stream()
      .filter(file -> !fileNamesInDriveFolder.contains(file.getName()))
      .collect(toList());
    
    if (filteredFiles.isEmpty()) {
      throw new GException(null, "Les fichiers ont déjà dans ce dossier Drive.");
    }
    
    for (java.io.File file : filteredFiles) {
        uploadSingleFile(file);
    }
    
    LOGGER.log(Level.INFO, "Your files was uploaded to your Google Drive successfully.");
  }
  
  /**
   * Vérifier si un fichier est une feuille de calcul.
   * 
   * <p>
   *   Un fichier est considéré comme une feuille de calcul si
   *   son extension est parmi l'ensemble (xlsx, xls, csv).
   * </p>
   * 
   * @param fileName Le nom du fichier
   * 
   * @return true Si le fichier est une feuille de calcul, false sinon.
   */
  private boolean isSpreadsheetFile(String fileName) {
    String fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
    return fileExtension.equalsIgnoreCase("xlsx")
        || fileExtension.equalsIgnoreCase("xls")
        || fileExtension.equalsIgnoreCase("csv");
  }
  
  /**
   * Vérifier si un fichier est un document de texte.
   * 
   * <p>
   *   Un fichier est considéré comme un document de texte si
   *   son extension est parmi l'ensemble (docx, doc, odt).
   * </p>
   * 
   * @param fileName Le nom du fichier
   * 
   * @return true Si le fichier est un document de texte, false sinon.
   */
  private boolean isDocumentFile(String fileName) {
    String fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
    return fileExtension.equalsIgnoreCase("docx")
        || fileExtension.equalsIgnoreCase("doc")
        || fileExtension.equalsIgnoreCase("odt");
  }
  
  /**
   * Transférer un seul fichier vers ce dossier Drive. 
   * 
   * @param file Le fichier à transférer.
   * 
   * @throws GException Si un problème est apparue lors la création du fichier.
   * 
   * @see #isSpreadsheetFile(String)
   * @see #isDocumentFile(String)
   * @see #createExceptionForGDrive(GoogleJsonResponseException)
   */
  private void uploadSingleFile(java.io.File file) throws GException {
    String fileName = file.getName();
    
    File fileMetadata = new File();
    fileMetadata.setName(fileName);
    fileMetadata.setParents(Collections.singletonList(folderId));

    // Convertir le fichier vers Google Sheet s'il s'agit d'une feuille de calcul
    // Ou vers Google Doc s'il s'agit d'un document
    if (isSpreadsheetFile(fileName)) {
      fileMetadata.setMimeType("application/vnd.google-apps.spreadsheet");
    } else if (isDocumentFile(fileName)) {
      fileMetadata.setMimeType("application/vnd.google-apps.document");
    }
    
    java.io.File filePath = new java.io.File(file.toString());
    
    // Récupérer le MIME Type du fichier
    String fileMIMEType = null;
    try {
      fileMIMEType = Files.probeContentType(Paths.get(filePath.getAbsolutePath()));
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
    
    // Création du fichier
    FileContent mediaContent = new FileContent(fileMIMEType, filePath);
    try {
      service
        .files()
        .create(fileMetadata, mediaContent)
        .setFields("id, name")
        .execute();
    } catch (GoogleJsonResponseException e) {
      throw createExceptionForGDrive(e);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }
  
  /**
   * Créer une exception contenant un message utile pour l'utilisateur.
   * 
   * <p>
   *  Cette méthode extrait le code de retour depuis l'exception <i>GoogleJsonResponseException</i>,
   *  afin de créer une exception contenant un message correspond à ce code qui décrit bien le problème.
   * </p>
   * 
   * <p>Les messages d'erreurs qui peut contenir l'exception sont:</p>
   * 
   * <ul>
   *  <li>Une requête invalide a été envoyée!</li>
   *  <li>Vos crédentiels sont invalides. Veuillez rafraîchir votre access token.</li>
   *  <li>Vous n'avez pas la permission d'ouvrir le dossier avec l'identifiant (l'identifiant du dossier Drive)!</li>
   *  <li>N'existe aucun dossier dans votre Google Drive avec l'identifiant (l'identifiant du dossier Drive)!</li>
   *  <li>Vous avez envoyé trop de requêtes.</li>
   *  <li>Une erreur inattendue. Veuillez réssayer une autre fois.</li>
   * </ul>
   * 
   * @param e Une instance de GoogleJsonResponseException, qui correspond à une réponse du serveur.
   * 
   * @return Une instance de GException, qui correspond à une exception contenant un message utile pour l'utilisateur.
   * 
   * @see #folderId
   */
  private GException createExceptionForGDrive(GoogleJsonResponseException e) {
    // Récupérer la moitié de l'ID pour s'adapter à la largeur de la fenêtre de l'alerte
    String id = folderId.substring(0, folderId.length() + 1 / 2);
    
    if (e.getStatusCode() == 400) {
      return new GException(e, "Une requête invalide a été envoyée!");
    }
    
    if (e.getStatusCode() == 401) {
      return new GException(e, "Vos crédentiels sont invalides. Veuillez rafraîchir votre access token.");
    }
    
    if (e.getStatusCode() == 403) {
      return new GException(e, String.format("Vous n'avez pas la permission d'ouvrir le dossier avec l'identifiant (%s...)!", id));
    }
    
    if (e.getStatusCode() == 404) {
      return new GException(e, String.format("N'existe aucun dossier dans votre Google Drive avec l'identifiant (%s...)!", id));
    }
    
    if (e.getStatusCode() == 429) {
      return new GException(e, "Vous avez envoyé trop de requêtes.");
    }
    
    return new GException(e, "Une erreur inattendue. Veuillez réssayer une autre fois.");
  }
  
}
