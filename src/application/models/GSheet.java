package application.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

import application.validation.GException;

/**
 * <b>La classe GSheet est la classe représentant d'une feuille de calcul (Google Sheets).</b>
 * <p>Un GSheet est caractérisé par un identifiant unique.</p>
 * 
 * <p>
 *  Pour avoir l'accès aux feuilles de calculs (Google Sheets), il faudra que l'utilisateur s'authentifie
 *  à travers le service d'authentification qui permet l'accès aux Google Sheets.
 * </p>
 * 
 * @author Mohcine EL KASSIB, Abdrrezzak ATIFI
 * @since v1.0
 * 
 * @see Authentication
 */
public class GSheet {

  /**
   * Un logger utilisé pour afficher le journal d'événements.
   */
  private static final Logger LOGGER = Logger.getLogger(GSheet.class.getName());
  
  /**
   * Le service d'authentification qui permet l'accès aux feuilles de calculs.
   * 
   * <p>
   *  Il est définit comme un attribut {@code static} parce que toutes les instances
   *  de GSheet partage le même service d'authentification aux Google Sheets.
   * <p>
   * 
   * @see #getService()
   * @see #setService(Sheets)
   * @see Authentication#getSheetsService()
   */
  private static Sheets service;
  
  /**
   * L'identifiant unique de la feuille de calcul.
   * 
   * @see #GSheet(String)
   * @see #getSpreadsheetId()
   * @see #setSpreadsheetId(String)
   */
  private String spreadsheetId;

  /**
   * Les données de la feuille de calcul.
   * 
   * @see #getValues()
   * @see #setValues(Map)
   * @see #updateValues(Map)
   */
  private Map<List<String>, List<String>> spreadSheetvalues = new LinkedHashMap<>();
  
  /**
   * La liste de la barre de titre
   * 
   * @see #getTitleBar()
   * @see #setTitleBar(List)
   */
  private List<String> titleBar = new ArrayList<>();

  /**
   * Constructeur GSheet.
   * 
   * @param spreadsheetId L'identifiant de la feuille de calcul
   */
  public GSheet(String spreadsheetId) {
    this.spreadsheetId = spreadsheetId;
  }

  /**
   * Obtenir l'identifiant de la feuille de calcul.
   * 
   * @return {@link #spreadsheetId}
   * 
   * @see #spreadsheetId
   */
  public String getSpreadsheetId() {
    return spreadsheetId;
  }
  
  /**
   * Mettre à jour l'identifiant de la feuille de calcul.
   * 
   * @param spreadsheetId Le nouveau identifiant
   * 
   * @see #spreadsheetId
   */
  public void setSpreadsheetId(String spreadsheetId) {
    this.spreadsheetId = spreadsheetId;
  }
  
  /**
   * Obtenir le service d'accès aux spreadsheets (Google Sheets).
   * 
   * @return Le service d'accès aux spreadsheets (Google Sheets)
   * 
   * @see #service
   */
  public static Sheets getService() {
    return service;
  }
  
  /**
   * Mettre à jour le service d'accès aux spreadsheets (Google Sheets).
   * 
   * @param service Le nouveau service d'accès aux spreadsheets (Google Sheets)
   * 
   * @see #service
   */
  public static void setService(Sheets service) {
    GSheet.service = service;
  }

  /**
   * Obtenir les données de la feuille de calcul.
   * 
   * @return {@link #spreadSheetvalues}
   * 
   * @see #spreadSheetvalues
   */
  public Map<List<String>, List<String>> getValues() {
    return spreadSheetvalues;
  }

  /**
   * Initialiser la structure de données.
   * 
   * @param values Une structure de données
   * 
   * @see #spreadSheetvalues
   */
  public void setValues(Map<List<String>, List<String>> values) {
    this.spreadSheetvalues = values;
  }

  /**
   * Mettre à jour la barre de titre.
   * 
   * @param titleBar La nouvelle barre de titre
   * 
   * @see #titleBar
   */
  public void setTitleBar(List<String> titleBar) {
    this.titleBar = titleBar;
  }
  
  /**
   * Obtenir la barre de titre.
   * 
   * @return {@link #titleBar}
   * 
   * @see #titleBar
   */
  public List<String> getTitleBar() {
    return titleBar;
  }

  /**
   * Remplir la structure de données par les données de la feuille de calcul.
   * 
   * @param keysList    La liste des champs des clés
   * @param valuesList  La liste des champs des valeurs
   * @param hasTitleBar L'existance de la barre de titre
   * 
   * @throws GException Si la feuille de calcul est vide.
   *
   * @see #getSpreadsheetValues(String)
   * @see SheetUtilties#getValuesList(List, List)
   */
  public void collectInfo(List<Character> keysList, List<Character> valuesList, boolean hasTitleBar) throws GException {
    spreadSheetvalues.clear();

    LOGGER.log(Level.INFO, "Collecting values from ({0})...", spreadsheetId);

    List<List<Object>> rows = getSpreadsheetValues("A1:Z");
    
    if (hasTitleBar) {
      titleBar.addAll(SheetUtilties.getValuesList(rows.get(0), valuesList));
      rows.remove(0);
    }

    if (rows.isEmpty()) {
      throw new GException(null, "N'existe aucune donnée dans (" + spreadsheetId + ")");
    }
    
    for (List<Object> row : rows) {
      List<String> studentKey = SheetUtilties.getValuesList(row, keysList);

      if (valuesList.isEmpty()) {
        spreadSheetvalues.putIfAbsent(studentKey, Collections.emptyList());
      } else {
        List<String> studentValues = SheetUtilties.getValuesList(row, valuesList);
        spreadSheetvalues.putIfAbsent(studentKey, studentValues);
      }
    }
  }

  /**
   * Mettre à jour la structure de données avec les données d'une autre feuille de calcul.
   * 
   * @param otherValues Une structure de données
   * 
   * @see #spreadSheetvalues
   */
  public void updateValues(Map<List<String>, List<String>> otherValues) {
    otherValues.forEach((key, value) -> {
      if (spreadSheetvalues.containsKey(key)) {
        spreadSheetvalues.put(key, value);
      }
    });

    LOGGER.info("Values was updated successfully");
  }

  /**
   * Sauvegarder les données dans la feuille de calcul.
   * 
   * @param columnWhereToAdd Le champ de destination
   * @param titleBarList     La barre de titre
   * @param isKeyIncluded    L'ajout des clés
   * 
   * @throws GException
   * 
   * @see #updateSpreadsheetValues(String, ValueRange)
   */
  public void saveValues(char columnWhereToAdd, List<String> titleBarList, boolean isKeyIncluded) throws GException {
    LOGGER.log(Level.INFO, "Saving values in ({0}) ...", spreadsheetId);

    List<List<Object>> rowsToAdd = new ArrayList<>();
    rowsToAdd.add(new ArrayList<Object>(titleBarList));

    spreadSheetvalues.forEach((key, value) -> {
      List<Object> row = isKeyIncluded ? Arrays.asList(key.get(0), value.get(0)) : new ArrayList<>(value);
      rowsToAdd.add(row);
    });

    ValueRange body = new ValueRange().setValues(rowsToAdd);
    updateSpreadsheetValues(columnWhereToAdd + "1", body);
  }

  /**
   * Sauvegarder le calcul de nombre de présences des séance de classe.
   * 
   * @param spreadsheetsIds    La liste des identifiants des feuilles de calculs
   * @param sourceKey          Le champ de la clé source
   * @param sourceColumn       Le champ de nombre de présence
   * @param destinationSheetId L'identifiant de la feuille destination
   * 
   * @throws GException
   * 
   * @see #countNumberOfPresences(List, char, Character)
   * @see #setValues(Map)
   * @see #saveValues(char, List, boolean)
   */
  public static void saveAttendees(List<String> spreadsheetsIds, char sourceKey, Character sourceColumn, String destinationSheetId) throws GException {
    // Count number of presences first
    Map<List<String>, Integer> numberOfPresences = countNumberOfPresences(spreadsheetsIds, sourceKey, sourceColumn);

    // Build rows to append to the result sheet
    Map<List<String>, List<String>> sheetsRows = new LinkedHashMap<>();
    numberOfPresences.forEach((key, value) -> sheetsRows.put(key, Collections.singletonList(value.toString())));

    // Create an instance for the destination sheet
    GSheet destinationSheet = new GSheet(destinationSheetId);
    destinationSheet.setValues(sheetsRows);

    destinationSheet.saveValues('A', Arrays.asList("Code", "Nombre de présence"), true);
  }

  /**
   * Extraire les données depuis une feuille de calcul.
   * 
   * <p>
   *  Pour plus d'informations sur la notation A1:
   *  <a href="https://developers.google.com/sheets/api/guides/concepts#a1_notation" target="_blank">A1 Notation</a>
   * </p>
   * 
   * @param range La notation A1
   * 
   * @return Les données de la feuille de calcul
   * 
   * @throws GException
   */
  public List<List<Object>> getSpreadsheetValues(String range) throws GException {
    List<List<Object>> rowsData = null;
    
    try {
      rowsData = service.spreadsheets().values().get(spreadsheetId, range).execute().getValues();
    } catch (GoogleJsonResponseException e) {
      throw createExceptionForGSheet(e);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

    if (rowsData != null) {
      return rowsData;
    }
    
    return Collections.emptyList();
  }
  
  /**
   * Mettre à jour les données d'une feuille de calcul.
   * <p>
   *  Pour plus d'informations sur la notation A1:
   *  <a href="https://developers.google.com/sheets/api/guides/concepts#a1_notation" target="_blank">A1 Notation</a>
   * </p>
   * 
   * @param range La notation A1
   * @param body  Les données à sauvegarder
   * 
   * @throws GException
   */
  public void updateSpreadsheetValues(String range, ValueRange body) throws GException {
    try {
      service
        .spreadsheets()
        .values()
        .update(spreadsheetId, range, body)
        .setValueInputOption("RAW")
        .execute();
    } catch (GoogleJsonResponseException e) {
      throw createExceptionForGSheet(e);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }
  
  /**
   * Calculer le nombre de présences des séances de classe.
   * 
   * @param spreadsheetsIds La liste des identifiants des feuilles de calculs
   * @param sourceKey       Le champ de la clé source
   * @param sourceColumn    Le champ de nombre de présence
   * 
   * @return Le nombre de présences des séances de classe
   * 
   * @throws GException
   * 
   * @see #collectInfo(List, List, boolean)
   * @see #getValues()
   */
  private static Map<List<String>, Integer> countNumberOfPresences(List<String> spreadsheetsIds, char sourceKey, Character sourceColumn) throws GException {
    /*
     * S'il existe la colonne source(la colonne de nombre de présence), on va calculer le nombre des présences
     * globale, sinon on va calculer le nombre des présences par ensemble (TD, TP ou cours).
     */
    List<Character> srcValues = sourceColumn != null ? Collections.singletonList(sourceColumn) : Collections.emptyList();

    // Les feuilles de présences n'ont pas la barre de titre
    boolean hasTitleBar = sourceColumn != null ? true : false;

    List<GSheet> attendanceSheets = new ArrayList<>();
    for (String id : spreadsheetsIds) {
      GSheet attendanceSheet = new GSheet(id);
      attendanceSheet.collectInfo(Collections.singletonList(sourceKey), srcValues, hasTitleBar);
      attendanceSheets.add(attendanceSheet);
    }

    Map<List<String>, Integer> numberOfPresences = new LinkedHashMap<>();

    // Si on trouvé un code d'étudiant, on accumule le nombre de présence correspond à ce code,
    // sinon on initialise le nombre de présence par 1
    attendanceSheets.forEach(attendanceSheet ->
      attendanceSheet.getValues().forEach((key, value) -> {
        boolean isEmptyList = value.isEmpty();
        numberOfPresences.computeIfPresent(
            key,
            (k, oldValue) -> oldValue + (isEmptyList ? 1 : Integer.parseInt(value.get(0)))
        );

        numberOfPresences.putIfAbsent(key, isEmptyList ? 1 : Integer.parseInt(value.get(0)));
      })
    );

    return numberOfPresences;
  }

  /**
   * Créer une exception contenant un message utile pour l'utilisateur.
   * 
   * <p>
   *  Cette méthode extrait le code de retour depuis l'exception <i>GoogleJsonResponseException</i>,
   *  afin de créer une exception contenant un message correspond à ce code qui décrit bien le problème.
   * </p>
   * 
   * <p>Les messages d'erreurs qui peut contenir l'exception sont:</p>
   * 
   * <ul>
   *  <li>Une requête invalide a été envoyée!</li>
   *  <li>Vos crédentiels sont invalides. Veuillez rafraîchir votre access token.</li>
   *  <li>Vous n'avez pas la permission d'ouvrir le spreadsheet avec l'identifiant (l'identifiant du spreadsheet)!</li>
   *  <li>N'existe aucun spreadsheet avec l'identifiant (l'identifiant du spreadsheet)!</li>
   *  <li>Vous avez envoyé trop de requêtes.</li>
   *  <li>Une erreur inattendue. Veuillez réssayer une autre fois.</li>
   * </ul>
   * 
   * @param e Une instance de GoogleJsonResponseException, qui correspond à une réponse du serveur.
   * 
   * @return Une instance de GException, qui correspond à une exception contenant un message utile pour l'utilisateur.
   * 
   * @see #spreadsheetId
   */
  private GException createExceptionForGSheet(GoogleJsonResponseException e) {
    // Récupérer la moitié de l'ID pour s'adapter à la largeur de la fenêtre de l'alerte
    String id = spreadsheetId.substring(0, spreadsheetId.length() + 1 / 2);
    
    if (e.getStatusCode() == 400) {
      return new GException(e, "Une requête invalide a été envoyée!");
    }
    
    if (e.getStatusCode() == 401) {
      return new GException(e, "Vos crédentiels sont invalides. Veuillez rafraîchir votre access token.");
    }
    
    if (e.getStatusCode() == 403) {
      return new GException(e, String.format("Vous n'avez pas la permission d'ouvrir le spreadsheet avec l'identifiant (%s...)!", id));
    }
    
    if (e.getStatusCode() == 404) {
      return new GException(e, String.format("N'existe aucun spreadsheet avec l'identifiant (%s...)!", id));
    }
    
    if (e.getStatusCode() == 429) {
      return new GException(e, "Vous avez envoyés trop de requêtes.");
    }
    
    return new GException(e, "Une erreur inattendue. Veuillez réssayer une autre fois.");
  }
  
}
