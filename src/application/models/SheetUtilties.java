package application.models;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;

import application.validation.GException;

/**
 * <b>
 *  La classe SheetUtilities est une classe utilitaire qui permet d’extraire les
 *  données depuis les lignes des feuilles de calculs (Google Sheets).
 * </b>
 * 
 * @author Mohcine EL KASSIB
 * @since v1.0
 *
 * @see GSheet
 */
public class SheetUtilties {
  
  private SheetUtilties() {}
  
  /**
   * Extraire la liste des valeurs depuis une ligne des données correspond à une liste des champs.
   * 
   * @param row        La ligne des données
   * @param valuesList La liste des champs
   * 
   * @return La liste des valeurs
   * 
   * @throws GException - Si les champs ont dépassés l'interval des données
   * 
   * @see #getIndexesForColumns(List)
   */
  public static List<String> getValuesList(List<Object> row, List<Character> valuesList) throws GException {
    if (valuesList.isEmpty()) {
      return Collections.emptyList();
    }
    
    List<Integer> columnIndexesList = getIndexesForColumns(valuesList);
    int maxIndex = Collections.max(columnIndexesList);
    
    // Vérifier que les champs ne dépassent pas l'interval des données
    if (maxIndex > row.size()) {
      throw new GException(null, "Les champs ont dépassés l'interval des données.");
    }
    
    return columnIndexesList.stream().map(index -> (String)row.get(index)).collect(toList());
  }
  
  /**
   * Obtenir les indices d’une liste des champs.
   * 
   * @param keys La liste des champs
   * 
   * @return La liste des indices
   */
  private static List<Integer> getIndexesForColumns(List<Character> keys) { 
    return keys.stream().map(key -> key - 'A').collect(toList());
  }
  
}
