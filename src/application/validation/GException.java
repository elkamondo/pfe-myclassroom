package application.validation;

public class GException extends Exception {

  private static final long serialVersionUID = 348420646134962961L;
  
  private final Exception error;
  
  private final String message;

  public GException(Exception error, String message) {
    this.error = error;
    this.message = message;
  }
  
  public String getErrorMessage() {
    return message;
  }

  @Override
  public String getMessage() {
    return error.getMessage();
  }
  
}
